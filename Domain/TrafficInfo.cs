﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestApp.Domain
{
	public class TrafficInfo
	{
		/// <summary>
		/// Gets or sets the region identifier.
		/// </summary>
		/// <value>
		/// The region identifier.
		/// </value>
		public int RegionId { get; set; }

		/// <summary>
		/// Gets or sets the level.
		/// </summary>
		/// <value>
		/// The level.
		/// </value>
		public byte Level { get; set; }

		/// <summary>
		/// Gets or sets the hint.
		/// </summary>
		/// <value>
		/// The hint.
		/// </value>
		public string Hint { get; set; }
	}
}
