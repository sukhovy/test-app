﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using TestApp.BLL.Services.Abstract;
using TestApp.Domain;

namespace TestApp.WebService.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	public class RegionsController : ControllerBase
	{
		/// <summary>
		/// The yandex regions service
		/// </summary>
		private readonly IYandexRegionsService _yandexRegionsService
			;

		/// <summary>
		/// Initializes a new instance of the <see cref="RegionsController"/> class.
		/// </summary>
		/// <param name="yandexRegionsService">The yandex regions service.</param>
		public RegionsController(IYandexRegionsService yandexRegionsService)
		{
			_yandexRegionsService = yandexRegionsService;
		}

		/// <summary>
		/// Gets the list of regions.
		/// </summary>
		[HttpGet]
		public async Task<IList<YandexRegion>> Get()
		{
			return await _yandexRegionsService.GetAllRegionsAsync();
		}
	}
}
