﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using TestApp.BLL.Services.Abstract;
using TestApp.Domain;

namespace TestApp.WebService.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	public class TrafficController : ControllerBase
	{
		/// <summary>
		/// The traffic service
		/// </summary>
		private readonly ITrafficService _trafficService;

		/// <summary>
		/// Initializes a new instance of the <see cref="TrafficController"/> class.
		/// </summary>
		/// <param name="trafficService">The traffic service.</param>
		public TrafficController(ITrafficService trafficService)
		{
			_trafficService = trafficService;
		}

		/// <summary>
		/// Get the trafic info for all regions.
		/// </summary>
		[HttpGet]
		public async Task<IEnumerable<TrafficInfo>> Get()
		{
			return await _trafficService.GetForAllRegionsAsync();
		}

		/// <summary>
		/// Get the traffic info for region.
		/// </summary>
		[HttpGet("{id}", Name = "Get")]
		public async Task<TrafficInfo> Get(int id)
		{
			return await _trafficService.GetForRegionAsync(id);
		}
	}
}
