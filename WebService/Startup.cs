﻿using System;
using System.Data.SqlClient;
using DataAccess;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using StackExchange.Redis;
using TestApp.BLL.Caching;
using TestApp.BLL.Services.Abstract;
using TestApp.BLL.Services.Concrete;
using TestApp.BLL.TrafficDataProviders;
using TestApp.Domain;
using TestApp.WebService.Config;

namespace TestApp.WebService
{
	public class Startup
	{
		public Startup(IConfiguration configuration)
		{
			Configuration = configuration;
		}

		public IConfiguration Configuration { get; }

		// This method gets called by the runtime. Use this method to add services to the container.
		public void ConfigureServices(IServiceCollection services)
		{
			services.AddDbContextPool<ApplicationContext>(options =>
				options.UseSqlServer(GetConnectionString()));

			services.AddTransient<IYandexRegionsService, YandexRegionsService>();
			services.AddTransient<ITrafficService, CachedTrafficService>();

			var config = new ProvidersConfig();
			Configuration.GetSection("Providers").Bind(config);

			switch (config.TrafficDataSource)
			{
				case TrafficDataSource.Yandex:
					services.AddTransient<ITrafficDataProvider, YandexTrafficDataProvider>(x => new YandexTrafficDataProvider(config.YandexApiUrl));
					break;
				case TrafficDataSource.Fake:
					services.AddTransient<ITrafficDataProvider, FakeTrafficDataProvider>();
					break;
				default:
					throw new ArgumentOutOfRangeException();
			}

			var cacheLiveTime = TimeSpan.FromMinutes(config.CacheLivetimeInMinutes);

			switch (config.CachingProvider)
			{
				case CachingProvider.Redis:
					var redis = ConnectionMultiplexer.Connect(config.RedisInstance);
					var db = redis.GetDatabase();
					services.AddSingleton<ICachingProvider<int, TrafficInfo>, RedisCachingProvider<TrafficInfo>>(x => new RedisCachingProvider<TrafficInfo>(db, cacheLiveTime));
					break;
				case CachingProvider.InProc:
					services.AddSingleton<ICachingProvider<int, TrafficInfo>, InProcCachingProvider<int, TrafficInfo>>(x => new InProcCachingProvider<int, TrafficInfo>(cacheLiveTime));
					break;
				default:
					throw new ArgumentOutOfRangeException();
			}

			
			services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
		}

		// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
		public void Configure(IApplicationBuilder app, IHostingEnvironment env)
		{
			if (env.IsDevelopment())
			{
				app.UseDeveloperExceptionPage();
			}

			app.UseMvc();
		}

		private string GetConnectionString()
		{
			var dbConfig = new DatabaseConfig();
			Configuration.GetSection("Database").Bind(dbConfig);
			var csBuilder = new SqlConnectionStringBuilder
			{
				DataSource = dbConfig.DataSource,
				InitialCatalog = dbConfig.InitialCatalog,
				UserID = dbConfig.UserId, //stored as user secret
				Password = dbConfig.Password //stored as user secret
			};
			return csBuilder.ToString();
		}
	}
}
