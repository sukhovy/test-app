﻿namespace TestApp.WebService.Config
{
	public class ProvidersConfig
	{
		public TrafficDataSource TrafficDataSource { get; set; }
		public CachingProvider CachingProvider { get; set; }
		public int CacheLivetimeInMinutes { get; set; }
		public string YandexApiUrl { get; set; }
		public string RedisInstance { get; set; }
	}
}