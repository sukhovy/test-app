﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TestApp.WebService.Config
{
	public enum CachingProvider
	{
		Redis = 1,
		InProc = 2
	}
}
