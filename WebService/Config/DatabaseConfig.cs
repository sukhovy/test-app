﻿namespace TestApp.WebService.Config
{
	public class DatabaseConfig
	{
		public string DataSource { get; set; }
		public string InitialCatalog { get; set; }
		public string UserId { get; set; }
		public string Password { get; set; }
	}
}
