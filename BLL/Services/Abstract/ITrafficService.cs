﻿using System.Collections.Generic;
using System.Threading.Tasks;
using TestApp.Domain;

namespace TestApp.BLL.Services.Abstract
{
	public interface ITrafficService
	{
		/// <summary>
		/// Gets the traffic infor for all regions.
		/// </summary>
		Task<IList<TrafficInfo>> GetForAllRegionsAsync();

		/// <summary>
		/// Gets traffic info for region.
		/// </summary>
		/// <param name="yandexRegionId">The Yandex region id.</param>
		Task<TrafficInfo> GetForRegionAsync(int yandexRegionId);
	}
}
