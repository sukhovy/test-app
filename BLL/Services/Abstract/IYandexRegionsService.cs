﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TestApp.Domain;

namespace TestApp.BLL.Services.Abstract
{
	public interface IYandexRegionsService
	{
		/// <summary>
		/// Gets all regions.
		/// </summary>
		Task<IList<YandexRegion>> GetAllRegionsAsync();
	}
}
