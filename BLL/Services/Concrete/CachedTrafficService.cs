﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DataAccess;
using Microsoft.EntityFrameworkCore;
using TestApp.BLL.Caching;
using TestApp.BLL.Services.Abstract;
using TestApp.BLL.TrafficDataProviders;
using TestApp.Domain;

namespace TestApp.BLL.Services.Concrete
{
	public class CachedTrafficService : ITrafficService
	{
		/// <summary>
		/// The application context
		/// </summary>
		private readonly ApplicationContext _applicationContext;

		/// <summary>
		/// The traffic data provider
		/// </summary>
		private readonly ITrafficDataProvider _trafficDataProvider;

		/// <summary>
		/// The cache
		/// </summary>
		private readonly ICachingProvider<int, TrafficInfo> _cache;

		/// <summary>
		/// Initializes a new instance of the <see cref="CachedTrafficService"/> class.
		/// </summary>
		/// <param name="applicationContext">The application context.</param>
		/// <param name="trafficDataProvider">The traffic data provider.</param>
		/// <param name="cache">The cache</param>
		public CachedTrafficService(ApplicationContext applicationContext, ITrafficDataProvider trafficDataProvider, ICachingProvider<int, TrafficInfo> cache)
		{
			_applicationContext = applicationContext;
			_trafficDataProvider = trafficDataProvider;
			_cache = cache;
		}

		/// <summary>
		/// Gets the traffic infor for all regions.
		/// </summary>
		public async Task<IList<TrafficInfo>> GetForAllRegionsAsync()
		{
			var allRegions = await _applicationContext.YandexRegions
				.ToListAsync();

			return allRegions
				.Select(x => GetForRegionAsync(x.Id))
				.Select(x => x.Result)
				.ToList();
		}

		/// <summary>
		/// Gets traffic info for region.
		/// </summary>
		/// <param name="yandexRegionId">The Yandex region id.</param>
		public async Task<TrafficInfo> GetForRegionAsync(int yandexRegionId)
		{
			var cached = _cache.Get(yandexRegionId);

			if (cached != null)
			{
				return cached;
			}

			var result = await _trafficDataProvider.GetTrafficInfoAsync(yandexRegionId);

			_cache.Set(yandexRegionId, result);

			return result;
		}
	}
}
