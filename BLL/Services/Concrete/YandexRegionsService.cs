﻿using System.Collections.Generic;
using System.Threading.Tasks;
using DataAccess;
using Microsoft.EntityFrameworkCore;
using TestApp.BLL.Services.Abstract;
using TestApp.Domain;

namespace TestApp.BLL.Services.Concrete
{
	public class YandexRegionsService : IYandexRegionsService
	{
		/// <summary>
		/// The application context
		/// </summary>
		private readonly ApplicationContext _applicationContext;

		/// <summary>
		/// Initializes a new instance of the <see cref="YandexRegionsService"/> class.
		/// </summary>
		/// <param name="applicationContext">The application context.</param>
		public YandexRegionsService(ApplicationContext applicationContext)
		{
			_applicationContext = applicationContext;
		}

		/// <summary>
		/// Gets all regions.
		/// </summary>
		public async Task<IList<YandexRegion>> GetAllRegionsAsync()
		{
			return await _applicationContext.YandexRegions
				.ToListAsync();
		}
	}
}
