﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TestApp.Domain;

namespace TestApp.BLL.TrafficDataProviders
{
	public class FakeTrafficDataProvider : ITrafficDataProvider
	{
		/// <summary>
		/// The random
		/// </summary>
		private readonly Random _random;

		/// <summary>
		/// Initializes a new instance of the <see cref="FakeTrafficDataProvider"/> class.
		/// </summary>
		public FakeTrafficDataProvider()
		{
			_random = new Random();
		}

		/// <summary>
		/// Gets the traffic information.
		/// </summary>
		/// <param name="yandexRegionId">The yandex region id.</param>
		public Task<TrafficInfo> GetTrafficInfoAsync(int yandexRegionId)
		{
			return Task.FromResult(new TrafficInfo()
			{
				RegionId = yandexRegionId,
				Level = (byte)_random.Next(10),
			});
		}
	}
}
