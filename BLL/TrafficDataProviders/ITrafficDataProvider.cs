﻿using System;
using System.Threading.Tasks;
using TestApp.Domain;

namespace TestApp.BLL.TrafficDataProviders
{
	public interface ITrafficDataProvider
	{
		/// <summary>
		/// Gets the traffic information.
		/// </summary>
		/// <param name="yandexRegionId">The yandex region id.</param>
		/// <returns></returns>
		Task<TrafficInfo> GetTrafficInfoAsync(int yandexRegionId);
	}
}
