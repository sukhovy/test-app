﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using System.Xml;
using Microsoft.EntityFrameworkCore.Internal;
using TestApp.Domain;

namespace TestApp.BLL.TrafficDataProviders
{
	public class YandexTrafficDataProvider : ITrafficDataProvider
	{
		/// <summary>
		/// The yandex service URL
		/// </summary>
		private readonly string _yandexServiceUrl;

		/// <summary>
		/// Initializes a new instance of the <see cref="YandexTrafficDataProvider"/> class.
		/// </summary>
		public YandexTrafficDataProvider(string yandexServiceUrl)
		{
			_yandexServiceUrl = yandexServiceUrl;
		}

		/// <summary>
		/// Gets the traffic information.
		/// </summary>
		/// <param name="yandexRegionId">The yandex region id.</param>
		public async Task<TrafficInfo> GetTrafficInfoAsync(int yandexRegionId)
		{
			using (var client = new HttpClient())
			{
				var timestamp = ((DateTimeOffset)DateTime.Now).ToUnixTimeSeconds();
				var response = await client.GetAsync(string.Format(_yandexServiceUrl, yandexRegionId, timestamp));
				if (!response.IsSuccessStatusCode)
				{
					return null;
				}
				var xmlDoc = new XmlDocument();
				using (var responseStream = await response.Content.ReadAsStreamAsync())
				{
					xmlDoc.Load(responseStream);
					const string xpath = @"info/traffic/region";
					var nodes = xmlDoc.SelectNodes(xpath);
					if (nodes == null || !nodes.Any())
					{
						return null;
					}

					var result = Parse(nodes[0]);
					result.RegionId = yandexRegionId;
					return result;

				}
			}
		}

		private TrafficInfo Parse(XmlNode xmlNode)
		{
			return new TrafficInfo()
			{
				Level = byte.Parse(xmlNode.SelectNodes("level")[0].InnerText),
				Hint = xmlNode.SelectNodes("hint")[0].InnerText,
			};
		}
	}
}
