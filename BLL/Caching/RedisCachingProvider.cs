﻿using System;
using Newtonsoft.Json;
using StackExchange.Redis;

namespace TestApp.BLL.Caching
{
	public class RedisCachingProvider<TValue> : ICachingProvider<int, TValue> where TValue: class 
	{
		/// <summary>
		/// The database
		/// </summary>
		private readonly IDatabase _database;

		/// <summary>
		/// The expiration time
		/// </summary>
		private readonly TimeSpan _expirationTime;

		/// <summary>
		/// Initializes a new instance of the <see cref="RedisCachingProvider{TValue}"/> class.
		/// </summary>
		/// <param name="database">The database.</param>
		/// <param name="expirationTime">The expiration time.</param>
		public RedisCachingProvider(IDatabase database, TimeSpan expirationTime)
		{
			_database = database;
			_expirationTime = expirationTime;
		}

		/// <summary>
		/// Gets the specified key.
		/// </summary>
		/// <param name="key"></param>
		public TValue Get(int key)
		{
			var val = _database.StringGet(key.ToString());
			if (val.IsNull)
			{
				return null;
			}
			var json = val.ToString();
			return JsonConvert.DeserializeObject<TValue>(json);
		}

		/// <summary>
		/// Sets the specified key.
		/// </summary>
		/// <param name="key"></param>
		/// <param name="value"></param>
		public void Set(int key, TValue value)
		{
			var json = JsonConvert.SerializeObject(value);
			_database.StringSet(key.ToString(), json, _expirationTime);
		}
	}
}
