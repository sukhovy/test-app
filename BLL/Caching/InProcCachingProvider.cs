﻿using System;
using System.Collections.Concurrent;
using System.Linq;

namespace TestApp.BLL.Caching
{
	public class InProcCachingProvider<TKey, TValue> : ICachingProvider<TKey, TValue>
	{
		/// <summary>
		/// The cache
		/// </summary>
		private readonly ConcurrentDictionary<TKey, (DateTime ExpiredIn, TValue Value)> _cache;

		/// <summary>
		/// The expiration time
		/// </summary>
		private readonly TimeSpan _expirationTime;

		/// <summary>
		/// Initializes a new instance of the <see cref="InProcCachingProvider{TKey, TValue}"/> class.
		/// </summary>
		/// <param name="expirationTime">The expiration time.</param>
		public InProcCachingProvider(TimeSpan expirationTime)
		{
			_cache = new ConcurrentDictionary<TKey, (DateTime, TValue)>();
			_expirationTime = expirationTime;
		}

		/// <summary>
		/// Gets the specified key.
		/// </summary>
		/// <param name="key">The key.</param>
		public TValue Get(TKey key)
		{
			if (!_cache.TryGetValue(key, out var value))
			{
				return default(TValue);
			}

			if (value.ExpiredIn < DateTime.Now)
			{
				_cache.TryRemove(key, out _);
			}
			else
			{
				return value.Value;
			}

			return default(TValue);
		}

		/// <summary>
		/// Sets the specified key.
		/// </summary>
		/// <param name="key">The key.</param>
		/// <param name="value">The value.</param>
		public void Set(TKey key, TValue value)
		{
			foreach (var existingKey in _cache.Keys.ToList())
			{
				if (_cache.TryGetValue(existingKey, out var existingValue) && existingValue.ExpiredIn < DateTime.Now)
				{
					_cache.TryRemove(existingKey, out _);
				}
			}
			var expiredIn = DateTime.Now + _expirationTime;
			_cache[key] = (expiredIn, value);
		}
	}
}
