﻿using System;

namespace TestApp.BLL.Caching
{
	public interface ICachingProvider<in TKey, TValue>
	{
		/// <summary>
		/// Gets the specified key.
		/// </summary>
		TValue Get(TKey key);

		/// <summary>
		/// Sets the specified key.
		/// </summary>
		void Set(TKey key, TValue value);
	}
}
