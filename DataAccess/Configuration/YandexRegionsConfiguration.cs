﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using TestApp.Domain;

namespace DataAccess.Configuration
{
	internal class YandexRegionsConfiguration : IEntityTypeConfiguration<YandexRegion>
	{
		public void Configure(EntityTypeBuilder<YandexRegion> builder)
		{
			builder.ToTable("yandex_regions");
			builder.HasKey(x => x.Id);
			builder.Property(x => x.Id).HasColumnName("id").IsRequired();
			builder.Property(x => x.Name).HasColumnName("name").IsRequired();
		}
	}
}
